package com.accenture.puntodos.commons.domains.generic;

import java.io.Serializable;

import lombok.Data;

@Data
public class VehiculoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String placa;
	private String marca;
	private String linea;
	private int modelo;
	private int cilindrada;
	private String color;
}
