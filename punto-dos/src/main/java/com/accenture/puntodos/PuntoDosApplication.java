package com.accenture.puntodos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PuntoDosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PuntoDosApplication.class, args);
	}
}
