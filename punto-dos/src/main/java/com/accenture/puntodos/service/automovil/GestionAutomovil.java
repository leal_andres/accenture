package com.accenture.puntodos.service.automovil;

import java.util.ArrayList;

import com.accenture.puntodos.commons.domains.request.AutomovilDTO;

public interface GestionAutomovil {
	
	public void createAutomovil(AutomovilDTO automovil);
	
	public AutomovilDTO findByPlacaAutomovil(String placa);
	
	public ArrayList<AutomovilDTO> findAllAutomovil();
}
