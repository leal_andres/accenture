package com.accenture.puntodos.service.automovil;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.accenture.puntodos.commons.domains.request.AutomovilDTO;

@Service
public class GestionAutomovilImpl implements GestionAutomovil {
	
	List<AutomovilDTO> automoviles = new ArrayList<>();

	@Override
	public void createAutomovil(AutomovilDTO automovil) {
		automoviles.add(automovil);
	}

	@Override
	public AutomovilDTO findByPlacaAutomovil(String placa) {
		for(AutomovilDTO automovilDTO: automoviles) {
			if (automovilDTO.getPlaca().equals(placa)) {
				return automovilDTO;
			}
		}
		return null;
	}

	@Override
	public ArrayList<AutomovilDTO> findAllAutomovil() {
		return (ArrayList<AutomovilDTO>) automoviles;
	}

}
