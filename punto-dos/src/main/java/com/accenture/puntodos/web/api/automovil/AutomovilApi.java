package com.accenture.puntodos.web.api.automovil;

import org.springframework.http.ResponseEntity;

import com.accenture.puntodos.commons.domains.request.AutomovilDTO;

public interface AutomovilApi {

	public ResponseEntity<?> createAutomovil(AutomovilDTO automovil);
	
	public ResponseEntity<?> findByPlacaAutomovil(String placa);
	
	public ResponseEntity<?> findAllAutomovil();
	
}
