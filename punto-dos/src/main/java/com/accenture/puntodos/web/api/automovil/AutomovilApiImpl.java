package com.accenture.puntodos.web.api.automovil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.puntodos.commons.domains.request.AutomovilDTO;
import com.accenture.puntodos.service.automovil.GestionAutomovilImpl;

@RestController
@RequestMapping("/api/vehiculos/automoviles")
public class AutomovilApiImpl implements AutomovilApi {
	
	@Autowired
	GestionAutomovilImpl gestionAutomovil;
	
	@Override
	@PostMapping("/")
	public ResponseEntity<?> createAutomovil(@RequestBody AutomovilDTO automovil) {
		gestionAutomovil.createAutomovil(automovil);
		return ResponseEntity.ok(automovil);
	}

	@Override
	@GetMapping("/{placa}")
	public ResponseEntity<?> findByPlacaAutomovil(@PathVariable("placa") String placa) {
		AutomovilDTO automovil = gestionAutomovil.findByPlacaAutomovil(placa);
		if (automovil == null) {
			return ResponseEntity.notFound().build();
		}else 
			return ResponseEntity.ok(automovil);
	}

	@Override
	@GetMapping("/")
	public ResponseEntity<?> findAllAutomovil() {
		if(gestionAutomovil.findAllAutomovil().size() == 0) {
			return ResponseEntity.noContent().build();
		}else 
			return ResponseEntity.ok(gestionAutomovil.findAllAutomovil());
	}
	
}
