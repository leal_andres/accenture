package com.accenture.puntotres.web.api.rest.user;

import com.accenture.puntotres.model.entities.User;
import org.springframework.http.ResponseEntity;


public interface UserApi {

    ResponseEntity createUser(User user);

    ResponseEntity getUserById(String id);

    ResponseEntity getAllUsers();

    ResponseEntity updateUser(String id, User user);

    ResponseEntity deleteUser(String id);
}
