package com.accenture.puntotres.web.api.rest.user;

import com.accenture.puntotres.commons.constans.api.user.EndpointUserApi;
import com.accenture.puntotres.commons.domains.response.builder.ResponseBuilder;
import com.accenture.puntotres.model.entities.User;
import com.accenture.puntotres.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = EndpointUserApi.USER_API)
public class UserApiImpl implements UserApi{

    @Autowired
    private UserService userService;

    @Override
    @PostMapping(EndpointUserApi.CREATE_USER)
    public ResponseEntity createUser(@RequestBody User user) {
        return ResponseBuilder.newBuilder()
                .withStatus(HttpStatus.CREATED)
                .withResponse(userService.createUser(user))
                .buildResponse();
    }

    @Override
    @GetMapping(EndpointUserApi.GET_USER_BY_ID)
    public ResponseEntity getUserById(@PathVariable("id") String id) {
        Optional<User> optUser = userService.getUserById(id);
        return ResponseBuilder.newBuilder()
                .withStatus(optUser.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND)
                .withResponse(optUser.isPresent() ? optUser.get() : null)
                .withMessage(optUser.isPresent() ? null : "User not has been found")
                .buildResponse();
    }

    @Override
    @GetMapping(EndpointUserApi.GET_ALL_USERS)
    public ResponseEntity getAllUsers() {
        return ResponseBuilder.newBuilder()
                .withStatus(HttpStatus.OK)
                .withResponse(userService.getAllUsers())
                .buildResponse();
    }

    @Override
    @PutMapping(EndpointUserApi.UPDATE_USER)
    public ResponseEntity updateUser(@PathVariable("id") String id, @RequestBody User user) {
        user.setId(id);
        Optional optUser = userService.updateUser(user);
        return ResponseBuilder.newBuilder()
                .withStatus(optUser.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND)
                .withResponse(optUser.isPresent() ? userService.updateUser(user) : null)
                .withMessage(optUser.isPresent() ? null : "User not has been found")
                .buildResponse();
    }

    @Override
    @DeleteMapping(EndpointUserApi.DELETE_USER)
    public ResponseEntity deleteUser(@PathVariable("id") String id) {
        userService.deleteUser(id);
        return ResponseBuilder.newBuilder()
                .withStatus(HttpStatus.OK)
                .withMessage("User has been deleted")
                .buildResponse();
    }
}
