package com.accenture.puntotres.commons.constans.api.user;

public interface EndpointUserApi {
    String USER_API = "users/";
    String CREATE_USER = "/";
    String GET_USER_BY_ID = "/{id}";
    String GET_ALL_USERS = "/";
    String UPDATE_USER = "/{id}";
    String DELETE_USER = "/{id}";
}
