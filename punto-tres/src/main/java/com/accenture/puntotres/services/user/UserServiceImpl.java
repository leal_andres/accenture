package com.accenture.puntotres.services.user;

import com.accenture.puntotres.model.entities.User;
import com.accenture.puntotres.repository.user.impl.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public Optional createUser(User user) {
        return Optional.of(repository.createUser(user));
    }

    @Override
    public Optional getUserById(String id) {
        Optional<User> optionalUser = repository.getUserById(id);
        if(optionalUser.isPresent()){
            return optionalUser;
        }else
            return Optional.empty();
    }

    @Override
    public Optional getAllUsers() {
        return Optional.of(repository.getAllUsers());
    }

    @Override
    public Optional updateUser(User user) {
        Optional<User> optionalUser = repository.getUserById(user.getId());
        if(optionalUser.isPresent()){
            return Optional.of(repository.updateUser(user));
        }else
            return Optional.empty();
    }

    @Override
    public void deleteUser(String id) {
        repository.deleteUser(id);
    }
}
