package com.accenture.puntotres.services.user;

import com.accenture.puntotres.model.entities.User;

import java.util.Optional;

public interface UserService {

    Optional createUser(User user);

    Optional getUserById(String id);

    Optional getAllUsers();

    Optional updateUser(User user);

    void deleteUser(String id);

}
