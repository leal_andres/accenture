package com.accenture.puntotres.repository.user.impl;

import com.accenture.puntotres.model.entities.User;
import com.accenture.puntotres.repository.user.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private IUserRepository repository;

    @Override
    public Optional<User> createUser(User user) {
        return Optional.of(repository.save(user));
    }

    @Override
    public Optional<User> getUserById(String id) {
        return repository.findById(id);
    }

    @Override
    public Optional<List<User>> getAllUsers() {
        return Optional.of(repository.findAll());
    }

    @Override
    public Optional<User> updateUser(User user) {
        return Optional.of(repository.save(user));
    }

    @Override
    public void deleteUser(String id) {
        repository.deleteById(id);
    }
}
