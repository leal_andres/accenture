package com.accenture.puntotres.repository.user.impl;

import com.accenture.puntotres.model.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    Optional<User> createUser(User user);

    Optional<User> getUserById(String id);

    Optional<List<User>> getAllUsers();

    Optional<User> updateUser(User user);

    void deleteUser(String id);

}
